<?php

/**
 * @file
 * Defines ems shipping method.
 */

module_load_include('inc', 'commerce_ems', 'includes/commerce_ems.connector'); 
 
/**
 * Implements hook_menu().
 */
function commerce_ems_menu() {
  $items = array();
  $items['admin/commerce/config/shipping/methods/ems-shipping-method/edit'] = array(
    'title' => 'Edit',
    'description' => 'Configure the Russian EMS Post shipping method.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_ems_settings_form'),
    'access arguments' => array('administer shipping'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 0,
    'file' => 'includes/commerce_ems.admin.inc',
  );
  
  return $items;
}

/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_ems_commerce_shipping_method_info() {
  $shipping_methods = array();
  $shipping_methods['ems_shipping_method'] = array(
    'title' => t('EMS Russian post'),
    'description' => t('Defines a single flat rate service (Russian Post) with a couple of service details.'),
  );

  return $shipping_methods;
}

/**
 * Implements hook_commerce_shipping_service_info().
 */
function commerce_ems_commerce_shipping_service_info() {
  $shipping_services = array();
  $shipping_services['ems_shipping_service'] = array(
    'title' => t('EMS Russian Post'),
    'description' => t('EMS Russian Post shipping service'),
    'display_title' => t('EMS shipping'),
    'shipping_method' => 'ems_shipping_method',
    'price_component' => 'shipping',
    'base_rate' => 0,
    'weight' => 10,
    'callbacks' => array(
      'rate' => 'commerce_ems_service_rate',
      'details_form' => 'commerce_ems_service_details_form',
      'details_form_validate' => 'commerce_ems_service_details_form_validate',
      'details_form_submit' => 'commerce_ems_service_details_form_submit',
    ),
  );
  return $shipping_services;
}

/**
 * Shipping service callback: returns a base price array for a shipping service
 * calculated for the given order.
 */
function commerce_ems_service_rate($shipping_service, $order) {
  // Return zero base rate
  return array(
    'amount' => 0,
    'currency_code' => 'RUB',
    'data' => array(),
  );
}

/**
 * Implements hook_theme().
 */
function commerce_ems_theme($existing, $type, $theme, $path) {
  return array(
    // Theme function for ems sgipping info
    'commerce_ems_service_details_note' => array(
      'template' => 'commerce-ems-service-details-note',
      'path' => drupal_get_path('module', 'commerce_ems') . '/theme',
      'variables' => array(
        'response' => NULL, 
        'price' => NULL,
        'period' => NULL
      ),
    ),
  );
}

/**
 * Implements template_preprocess_hook().
 */
function template_preprocess_commerce_ems_service_details_note(&$variables) {
  $variables['formatted_price'] = commerce_currency_format($variables['response']->price, commerce_default_currency());
  $variables['period'] = $variables['response']->period;
}

/**
 * Shipping service callback: returns the ems shipping service details form.
 * $_SESSION use for remember selected values for display right result if checkout page reload.
 */
function commerce_ems_service_details_form($pane_form, $pane_values, $checkout_pane, $order, $shipping_service) {
  $form = array();
   
  $commerce_ems_shipping_point = key(array_merge(commerce_ems_get_locations('cities'), commerce_ems_get_locations('regions')));  
  if (isset($pane_values['service_details']['commerce_ems_shipping_point'])) {
    $commerce_ems_shipping_point = $pane_values['service_details']['commerce_ems_shipping_point'];
  } 
  elseif (isset($_SESSION['commerce_ems']['commerce_ems_shipping_point'])) {
    $commerce_ems_shipping_point = $_SESSION['commerce_ems']['commerce_ems_shipping_point'];
  }

  $commerce_ems_options = 0;  
  if (isset($pane_values['service_details']['commerce_ems_options'])) {
    $commerce_ems_options = $pane_values['service_details']['commerce_ems_options'];
  } 
  elseif (isset($_SESSION['commerce_ems']['commerce_ems_options'])) {
    $commerce_ems_options = $_SESSION['commerce_ems']['commerce_ems_options'];
  }
  
  $pane_values['service_details'] += array(
    'commerce_ems_shipping_point' => '',
    'commerce_ems_options' => '',
    'commerce_ems_note' => '',
  );

  $ajax = array(
    'callback' => 'commerce_ems_service_details_ajax_reload',
    'event' => 'change',
    'progress' => array('type' => 'throbber', 'message' => t('Calculate EMS russian post shipping rate...')),
  );

  $form['commerce_ems_shipping_point'] = array(
    '#type' => 'select',
    '#title' => t('Shipping point'),
    '#options' => array(
      t('Cities') => commerce_ems_get_locations('cities'),
      t('States') => commerce_ems_get_locations('regions')
    ),
    '#description' => t('Please select you city or region delivery.'),
    '#default_value' => $commerce_ems_shipping_point,
    '#required' => FALSE,
    '#ajax' => $ajax
  );

  $form['commerce_ems_options'] = array(
    '#type' => 'radios',
    '#title' => t('Ems shipping type'),
    '#options' => array(
      '0' => t('Simple delivery'),
      '1' => t('Cash on delivery'),
    ),
    '#default_value' => $commerce_ems_options,
    '#ajax' => $ajax
  );

  $order_weight = commerce_ems_calculate_order_weight($order->order_id);

  $result = commerce_ems_calculate_rate(
    variable_get('commerce_ems_stock_location', 'city--moskva'), 
    $commerce_ems_shipping_point, 
    $order_weight,
    $commerce_ems_options, 
    $order
  );
  
  $form['commerce_ems_note'] = array(
    '#type' => 'markup',
    '#theme' => 'commerce_ems_service_details_note',
    '#response' => $result,
    '#price' => array(
      'amount' => $result->price,
      'currency_code' => 'RUB',
    ),
    '#period' => $result->period,
  );
  return $form;
}

/**
 * Calculate total weight amount of products in order.
 * Unit is kg.
 */
function commerce_ems_calculate_order_weight($order) {
  $order = entity_metadata_wrapper('commerce_order', $order);
  $weight = 0;
  foreach ($order->commerce_line_items as $delta => $line_item_wrapper) {
    $line_item = $line_item_wrapper->value();
    if  (in_array($line_item_wrapper->getBundle(), commerce_product_line_item_types())) {
      // TODO: integrate with commerce_physical fields ...
      $unit_weight = variable_get('commerce_ems_product_unit_weight', 0.5);
      // Altering unit_weight if we have custom logic of order weight amount calculation
      drupal_alter('commerce_ems_calculate_product_weight', $unit_weight, $order, $line_item);
      $weight += $unit_weight * $line_item_wrapper->quantity->value();
    }
  }
  return $weight;
}

/**
 * Implements hook_form_[form_id]_alter().
 * Altering checkout form.
 */
function commerce_ems_form_commerce_checkout_form_checkout_alter(&$form, &$form_state) {
  // Add a form class as base for css theming
  $form['#attributes']['class'][] = 'commerce-checkout-cart-checkout-form';
  
  $form['commerce_shipping']['shipping_service']['#attributes']['autocomplete'] = 'off';
  
  // Clear shipping method titile
  $method_title = $form['commerce_shipping']['shipping_service']['#options']['ems_shipping_service'];
  $form['commerce_shipping']['shipping_service']['#options']['ems_shipping_service'] = preg_replace('/\:.*/', '', $method_title);
  
  $form['buttons']['continue']['#validate'][] = 'commerce_ems_commerce_checkout_form_checkout_validate';
}

/**
 * See commerce_ems_form_commerce_checkout_form_checkout_alter defaned in this module.
 */
function _shipping_name_clear(&$val, $key) {
  $val = preg_replace('/\:.*/', '', $val);
}

/**
 * Checkout form validate function.
 * Check weight shipping package.
 */
function commerce_ems_commerce_checkout_form_checkout_validate($form, $form_state) {
  if ($form_state['input']['commerce_shipping']['shipping_service'] != 'ems_shipping_service') return;
  $order = $form_state['order'];
  $order_weight = commerce_ems_calculate_order_weight($order);
  if (commerce_ems_get_max_weight() < $order_weight)
    form_set_error('order', t('Sorry, max weight for Ems russian post is @max_weight kg', array('@max_weight' => commerce_ems_get_max_weight())));
}

/**
 * Update ems service detail form.
 */
function commerce_ems_service_details_ajax_reload(&$form, &$form_state) {
  $from = variable_get('commerce_ems_stock_location', 'city--moskva');
  $to = $form_state['input']['commerce_shipping']['service_details']['commerce_ems_shipping_point'];

  $order = $form_state['order'];
  
  $order_weight = commerce_ems_calculate_order_weight($order);
  $commerce_ems_options = $form_state['input']['commerce_shipping']['service_details']['commerce_ems_options'];
  
  $form['commerce_shipping']['service_details']['commerce_ems_note']['#response'] = commerce_ems_calculate_rate($from, $to, $order_weight, $commerce_ems_options == 1, $form_state['order']);
  
  // Remember selected values
  list($_SESSION['commerce_ems']['commerce_ems_shipping_point'], $_SESSION['commerce_ems']['commerce_ems_options']) = array($to, $commerce_ems_options);

  $form_state['rebuild'] = TRUE;
  $commands = array();
  $commands[] = ajax_command_replace('.commerce-checkout-cart-checkout-form', drupal_render($form));
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Ems shipping method validate
 */
function commerce_ems_service_details_form_validate($details_form, $details_values, $shipping_service, $order, $form_parents) {
  drupal_alter('commerce_ems_service_details_form_validate', $details_form, $details_values);
}

/**
 * Shipping service callback: increases the shipping line item's unit price if
 * express delivery was selected.
 */
function commerce_ems_service_details_form_submit($details_form, $details_values, $line_item) {
  if (!isset($details_values['commerce_ems_shipping_point'])) return;

  // Current order
  $order = commerce_order_load($line_item->order_id);  
  
  $from = variable_get('commerce_ems_stock_location', 'city--moskva');
  $to = $details_values['commerce_ems_shipping_point'];

  $all_locations =  array_merge(commerce_ems_get_locations('cities'), commerce_ems_get_locations('regions')); 
  
  $order_weight = commerce_ems_calculate_order_weight($order);  
 
  $result = commerce_ems_calculate_rate($from, $to, $order_weight, $details_values['commerce_ems_options'], $order);

  // Add new data at line item data
  $line_item->data['service_details_values'] =  array(
    'commerce_ems_shipping_point' =>  $all_locations[$details_values['commerce_ems_shipping_point']],
    'commerce_ems_options' => $details_form['commerce_ems_options']['#options'][$details_values['commerce_ems_options']],
  );
  // Save line item new data
  commerce_line_item_save($line_item);
  
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Current price format
  $currency = commerce_currency_load();

  // Build a price array for the express delivery fee.
  $ems_price = array(
    'amount' => $result->price,
    'currency_code' => $currency['code'],
    'data' => array(),
  );

  $line_item_wrapper->commerce_unit_price->amount = $result->price;

  // Add the ems delivery component to the unit price.
  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
    $line_item_wrapper->commerce_unit_price->value(),
    'ems_shipping_service',
    $ems_price,
    TRUE,
    FALSE
  );
}

/**
 * Implements hook_commerce_price_component_type_info().
 */
function commerce_ems_commerce_price_component_type_info() {
  return array(
    'ems_shipping_service' => array(
      'title' => t('Ems delivery'),
      'weight' => 1,
    ),
  );
}

/**
 * Implements hook_commerce_checkout_complete().
 * Clear all order session variables if order is complete
 */
function commerce_ems_commerce_checkout_complete($order) {
  if (isset($_SESSION['commerce_ems'])) 
    unset($_SESSION['commerce_ems']);
}


/**
 * Implements hook_field_extra_fields().
 */
function commerce_ems_field_extra_fields() {
  return array(
    'commerce_order' => array(
      'commerce_order' => array(
        'display' => array(
          'shipping_service_values' => array(
            'label' => t('Shipping service'),
            'description' => t('Shipping service width details (for ems)'),
            'weight' => 0,
          ),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_entity_view().
 */
function commerce_ems_entity_view($entity, $type, $view_mode, $langcode) {
  if ($type == 'commerce_order') {
    $entity->content['shipping_service_values'] = array(
	   '#markup' => token_replace('[commerce-order:order-shipping-method]', array('commerce-order' => $entity))
    );
  }
}