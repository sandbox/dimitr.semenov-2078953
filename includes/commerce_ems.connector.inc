<?php
/**
 * @file
 * Include main function for work with ems API (http://emspost.ru/ru/corp_clients/dogovor_docements/api/).
 */

define('DELIVERY_PERCENT', 0.01); 

/**
 * Calculate price and period to the point.
 */ 
function commerce_ems_calculate_rate($from, $to, $weight, $cash_on_delivery, $order) {
  $locations = array();
  $locations += commerce_ems_get_locations('regions');
  $locations += commerce_ems_get_locations('cities');

  if (!(in_array($from, array_keys($locations)) && in_array($to, array_keys($locations)))) {
    return (object) array(
      'status' => FALSE,
      'message' => t('One of required locations incorrect.')
    );
  }

  $request_url = 'http://emspost.ru/api/rest?method=ems.calculate&from=' . $from . '&to=' . $to . '&weight=' . $weight . '&type=att';
  $response = drupal_http_request($request_url);
  if ($response->code != 200) {
    return (object) array(
      'status' => FALSE,
      'message' => t('EMS post request failed, url @url returns unexpected response.', array('@url' => $request_url)),
		'price' => 0,
		'period' => 0
    );
  }

  $ems_data = json_decode($response->data);
  $ems_rates = $ems_data->rsp;

  if (!isset($ems_rates->price) || $ems_rates->price == 0) {
    return (object) array(
      'status' => FALSE,
      'message' => t('EMS Post returns zero shipping price at @url.', array('@url' => $request_url)),
		'price' => 0,
		'period' => 0		
    );
  }

  $currency = commerce_default_currency();
  if ($cash_on_delivery) {
    $base_price = 0;
	 // Calculate products price in order
    foreach (entity_metadata_wrapper('commerce_order', $order)->commerce_line_items as $delta => $line_item_wrapper) {
      $base_price += ($line_item_wrapper->getBundle() == 'product' && in_array($line_item_wrapper->getBundle(), commerce_product_line_item_types()))?$line_item_wrapper->commerce_total->amount->value():0;
    }
    $rate_price = commerce_currency_decimal_to_amount($ems_rates->price + ceil(($base_price * DELIVERY_PERCENT) / 100), $currency);
  } 
  else {
    $rate_price = commerce_currency_decimal_to_amount($ems_rates->price, $currency);
  }

  return (object) array(
    'status' => TRUE,
    'price' => $rate_price,
    'period' => $ems_rates->term->max,
  );  
  
}

/**
 * Ems list of city and regions.
 */
function commerce_ems_get_locations($type = 'regions') {
  if (!in_array($type, array('cities', 'regions'))) {
    return array();
  }

  // Look up cache
  if (FALSE !== ($cache = cache_get('commerce_ems_locations_' . $type))) {
    return $cache->data;
  }

  $request_url = 'http://emspost.ru/api/rest/?method=ems.get.locations&type=' . $type . '&plain=true';
  $response = drupal_http_request($request_url);

  if ($response->code != 200) {
    return array();
  }

  $ems_data = json_decode($response->data);
  $ems_locations = $ems_data->rsp->locations;

  $locations = array();

  foreach ($ems_locations as $location) {
    $locations[$location->value] = mb_convert_case($location->name, MB_CASE_TITLE);
  }
  
  //Clear Kazahstan from list
  if (isset($locations['region--kazahstan'])) {
    unset($locations['region--kazahstan']);
  } 
  
  if (count($locations)) {
    cache_set('commerce_ems_locations_' . $type, $locations, 'cache', 60*60*6); // 6 hours
  }

  return $locations;
}

/**
 * Calculate max package weight for Ems delivery (kg).
 */
function commerce_ems_get_max_weight() {
  $request_url = 'http://emspost.ru/api/rest/?method=ems.get.max.weight';
  $response = drupal_http_request($request_url);

  if ($response->code != 200) {
    return array();
  }

  $ems_weight_data = json_decode($response->data);
  return $ems_weight_data->rsp->max_weight;
}