<?php
/**
 * @file
 * Settings forms the for Commerce EMS Russian Post module.
 */


/**
 * Form callback: builds the settings form.
 *
 * Used to collect API credentials and enable supported shipping services.
 */
function commerce_ems_settings_form($form, &$form_state) {
  $form['commerce_ems_stock_location'] = array(
    '#type' => 'select',
    '#title' => t('Stock location'),
    '#description' => t('Choose stock location to calculate shipping rate.'),
    '#options' => array(
        t('Cities') => commerce_ems_get_locations('cities'),
        t('States') => commerce_ems_get_locations('regions')
    ),
    '#default_value' => variable_get('commerce_ems_stock_location', 'city--moskva'),
  );
  
  $form['commerce_ems_product_unit_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Product unit weight'),
    '#description' => t('Physical weight amount of product unit in kg.'),
    '#default_value' => variable_get('commerce_ems_product_unit_weight', '500'),
  );
  
  $form['commerce_ems_packege_max_weight'] = array(
    '#markup' => t('Max package weight for Ems: @weight kg', array('@weight' => commerce_ems_get_max_weight())), 
  );
  
  $form = system_settings_form($form);
  $form['actions']['submit']['#suffix'] = l(t('Cancel'), 'admin/commerce/config/shipping/methods');
  return $form;
}
