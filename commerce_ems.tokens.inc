<?php
/**
 * @file
 * Builds placeholder replacement tokens for order email.
 */

/**
 * Implements hook_token_info().
 */
function commerce_ems_token_info() {
  $type = array();
  $type[] = array(
    'name' => t('Orders', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Tokens related to email order letter'),
    'needs-data' => 'commerce-order',
  );

  // Tokens for orders.
  $order = array();
  
  // Order shipping method name width detail(comma separated) if ems selected (city and ems type). 
  $order['order-shipping-method'] = array(
    'name' => t('Commerce ems: Order shipping method width details.', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Order shipping method width details.'),
  );  

  // Return shipping price
  $order['order-shipping-method-amount'] = array(
    'name' => t('Commerce ems: Order shipping amount.', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Order shipping amount.'),
  );    
  
  return array(
   'types' => array('commerce-order' => $type),
   'tokens' => array('commerce-order' => $order),
  );  
}

/**
 * Implements hook_tokens().
 */
function commerce_ems_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);

  if (isset($options['language'])) {
   $url_options['language'] = $options['language'];
   $language_code = $options['language']->language;
  }
  else {
   $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'order-shipping-method':
          foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
            if ($line_item_wrapper->type->value() == 'shipping') {
              $line_item = $line_item_wrapper->value();
              $replacements[$original] = $line_item->line_item_label;
              if (isset($line_item->data['service_details_values'])) {
                $replacements[$original] .= ': ' . implode(', ', $line_item->data['service_details_values']);
              }
            }
          }
        break;
        case 'order-shipping-method-amount':
          foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
            if ($line_item_wrapper->type->value() == 'shipping') {
              $replacements[$original] = commerce_currency_format($line_item_wrapper->commerce_total->amount->value(), commerce_default_currency());
            }
          }
        break;      
      }
    }
  }

  return $replacements;
}