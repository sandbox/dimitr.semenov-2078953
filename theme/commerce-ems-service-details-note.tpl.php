<?php
/**
 * @file
 * Template file ems service details note.
 *
 * Available variables:
 * - $formatted_price - formatted price on delivery
 * - $period - delivery period
 */
?>
<div class="commerce-ems-service-details-note">
	<?php print t('EMS shipping price: @formatted_price', array('@formatted_price' => $formatted_price)); ?>, <?php print t('shipping period: @period', array('@period' => format_plural($period, '1 day', '@count days'))); ?>
</div>